package OnlineShoppingSystem.cleanCode.cart;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import OnlineShoppingSystem.cleanCode.items.Colors;
import OnlineShoppingSystem.cleanCode.items.Dress;
import OnlineShoppingSystem.cleanCode.items.Item;
import OnlineShoppingSystem.cleanCode.items.Size;

public class ShoppingCartTest {

    private ShoppingCart shoppingCart;
    
    @Before
    public void setUp() {
        shoppingCart = new ShoppingCart();
    }
    
    @Test
    public void givenEmptyShoppingCartWhenRemoveItemIsInvokedThenReturnFalse() {
        Item item = new Dress("Evening dress", 20.90f, Colors.RED,
                "Dress for the night", Size.M);
        
        assertFalse(shoppingCart.removeItem(item));
    }

    @Test
    public void givenItemWhichExistInCartWhenRemoveItemIsInvokedThenReturnTrue() {
        Item item = new Dress("Evening dress", 20.90f, Colors.RED,
                "Dress for the night", Size.M);
        shoppingCart.addItem(item);
        
        assertTrue(shoppingCart.removeItem(item));
    }
}
