package OnlineShoppingSystem.cleanCode;

import static org.junit.Assert.*;

import OnlineShoppingSystem.cleanCode.items.Colors;
import OnlineShoppingSystem.cleanCode.items.Dress;
import OnlineShoppingSystem.cleanCode.items.Item;
import OnlineShoppingSystem.cleanCode.items.Size;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MenuTest {

    private Menu menu;

    @Before
    public void setUp() {
        menu = new Menu();
    }

    @Test
    public void givenItemsWhenConstructItemMenuIsInvokedThenReturnMenuOptionsWithAllItems() {
        List<Item> givenItems = new ArrayList<Item>();

        Dress dress = new Dress("Evening dress", 20.90f, Colors.RED,
                "Dress for the night", Size.M);
        givenItems.add(dress);

        dress = new Dress("Morning dress", 120.90f, Colors.PINK,
                "Dress for the morning", Size.S);
        givenItems.add(dress);

        dress = new Dress("Prom dress", 50.99f, Colors.BLACK,
                "Evening prom dress", Size.L);
        givenItems.add(dress);

        dress = new Dress("Flower dress", 500.99f, Colors.BLACK,
                "Everyday flower dress", Size.L);
        givenItems.add(dress);

        dress = new Dress("Vintage dress", 10.99f, Colors.GREEN,
                "Unique comfortable dress", Size.XL);
        givenItems.add(dress);

        dress = new Dress("Club dress", 59.99f, Colors.BLACK,
                "Little black dress", Size.S);
        givenItems.add(dress);

        String expectedResult = "Items:\n" + "0: Evening dress\n"
                + "1: Morning dress\n" + "2: Prom dress\n" + "3: Flower dress\n"
                + "4: Vintage dress\n" + "5: Club dress\n"
                + "Eneter a number from 0 to 5: ";

        assertEquals(expectedResult, menu.constructItemMenu(givenItems));
    }


    @Test
    public void givenNoItemsWhenConstructItemMenuIsInvokedThenReturnEmptyString() {
        List<Item> givenItems = new ArrayList<Item>();

        String expectedResult = "";

        assertEquals(expectedResult, menu.constructItemMenu(givenItems));
    }

    @Test
    public void givenItemsWhenConstructRemoveItemMenuIsInvokedThenReturnMenuWithItemInformation() {
        List<Item> givenItems = new ArrayList<Item>();
        
        Dress dress = new Dress("Evening dress", 20.90f, Colors.RED,
                "Dress for the night", Size.M);
        givenItems.add(dress);
        
        dress = new Dress("Morning dress", 120.90f, Colors.PINK,
                "Dress for the morning", Size.S);
        givenItems.add(dress);
        
        String expectedResult = "";
        expectedResult = "Items in Shopping Cart:\n" + "ID: "
                + givenItems.get(0).getID() + "\nName: Evening dress"
                + "\nPrice: " + 20.90f + "\nDescription: Dress for the night"
                + "\nColor: RED" + "\nSize: M\n" + "ID: "
                + givenItems.get(1).getID() + "\nName: Morning dress"
                + "\nPrice: " + 120.90f + "\nDescription: Dress for the morning"
                + "\nColor: PINK" + "\nSize: S\n"
                + "Enter ID to remove product from ShoppingCart: ";
        
        assertEquals(expectedResult, menu.constructRemoveItemMenu(givenItems));
    }
    
    @Test
    public void givenNoItemsWhenConstructRemoveItemMenuIsInvokedThenReturnEmptyString() {
        List<Item> givenItems = new ArrayList<Item>();

        String expectedResult = "";

        assertEquals(expectedResult, menu.constructRemoveItemMenu(givenItems));
    }
}
