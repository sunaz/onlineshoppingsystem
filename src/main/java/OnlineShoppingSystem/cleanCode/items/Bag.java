package OnlineShoppingSystem.cleanCode.items;

public class Bag extends Item {

    private Types type;

    public Bag(String name, float price, Colors color, String description,
            Types type) {
        super(name, price, color, description);
        this.type = type;
    }

    @Override
    public String getInformation() {
        String information = "ID: " + id + "\nName: " + name + "\nPrice: "
                + price + "\nDescription: " + description + "\nColor: " + color
                + "\nType: " + type + "\n";
        return information;
    }

}
