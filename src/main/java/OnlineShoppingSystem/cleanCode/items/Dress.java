package OnlineShoppingSystem.cleanCode.items;

public class Dress extends Item {

    private Size size;

    public Dress(String name, float price, Colors color, String description,
            Size size) {
        super(name, price, color, description);
        this.size = size;
    }

    @Override
    public String getInformation() {
        String information = "ID: " + id + "\nName: " + name + "\nPrice: "
                + price + "\nDescription: " + description + "\nColor: " + color
                + "\nSize: " + size + "\n";
        return information;
    }

}
