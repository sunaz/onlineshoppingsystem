package OnlineShoppingSystem.cleanCode.items;

public enum Size {

    XS(1), S(2), M(3), L(4), XL(5), XXL(6);

    public final int numberOfSize = 6;
    private int value;

    private Size(int value) {
        this.value = value;
    }
}
