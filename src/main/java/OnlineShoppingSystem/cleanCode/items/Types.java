package OnlineShoppingSystem.cleanCode.items;

public enum Types {

    SHOULDER_BAG(1), HAND_BAG(2), BUCKET_BAG(3);
    
    private int value;
    
    private Types(int value) {
        this.value = value;
    }
}
