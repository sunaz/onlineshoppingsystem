package OnlineShoppingSystem.cleanCode.items;

public enum Colors {
    RED(1), GREEN(2), BLUE(3), PINK(4), BLACK(5);
    
    public final int numberOfColors = 5;
    private int colorValue;
    
    private Colors(int colorValue) {
        this.colorValue = colorValue;
    }
}
