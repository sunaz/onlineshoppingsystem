package OnlineShoppingSystem.cleanCode.items;

public enum ShoeSize {
    THIRTY_SIX(1), THIRTY_SEVEN(2), THIRTY_EIGHT(3), THIRTY_NINE(4), FORTY(5),
    FORTY_ONE(6);

    private int value;

    private ShoeSize(int value) {
        this.value = value;
    }
}
