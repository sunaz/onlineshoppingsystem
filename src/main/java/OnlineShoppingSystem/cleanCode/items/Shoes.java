package OnlineShoppingSystem.cleanCode.items;

public class Shoes extends Item {

    private ShoeSize size;

    public Shoes(String name, float price, Colors color, String description,
            ShoeSize size) {
        super(name, price, color, description);
        this.size = size;
    }

    @Override
    public String getInformation() {
        String information = "ID: " + id + "\nName: " + name + "\nPrice: "
                + price + "\nDescription: " + description + "\nColor" + color
                + "\nSize" + size + "\n";
        return information;
    }

}
