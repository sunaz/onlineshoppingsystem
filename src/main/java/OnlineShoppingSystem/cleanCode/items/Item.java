package OnlineShoppingSystem.cleanCode.items;

import java.util.UUID;

public abstract class Item {
    protected String id;
    protected String name;
    protected float price;
    protected Colors color;
    protected String description;

    public Item(String name, float price, Colors color, String description) {
        id = UUID.randomUUID().toString();
        this.name = name;
        this.price = price;
        this.color = color;
        this.description = description;
    }

    public abstract String getInformation();

    public String getName() {
        return name;
    }

    public String getID() {
        return id;
    }
}
