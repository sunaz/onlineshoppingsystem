package OnlineShoppingSystem.cleanCode;

public class Payment {

    Talk communicationChannel;

    public Payment(Talk communicationChannel) {
        this.communicationChannel = communicationChannel;
    }

    public void payWithDebitCard() {
        printPaymentMessage();
    }

    public void payWithCreditCard() {
        printPaymentMessage();
    }

    private void printPaymentMessage() {
        String paymentMessage = "Payment Success!";
        communicationChannel.print(paymentMessage);
    }
}
