package OnlineShoppingSystem.cleanCode;

import java.util.Scanner;

public class Talk {
    private Scanner scanner;

    public Talk() {
        scanner = new Scanner(System.in);
    }

    public int enterMenuOption(String menu, int minimum, int maximum) {
        Integer menuOption;
        do {
            print(menu);
            menuOption = scanner.nextInt();
        } while (menuOption < minimum || menuOption > maximum);
        return menuOption;
    }

    public void print(String message) {
        System.out.print(message);
    }

    public int getOption() {
        return scanner.nextInt();
    }

    public String getOptionID() {
        return scanner.next();
    }
}
