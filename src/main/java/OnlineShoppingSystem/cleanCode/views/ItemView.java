package OnlineShoppingSystem.cleanCode.views;

import OnlineShoppingSystem.cleanCode.Menu;
import OnlineShoppingSystem.cleanCode.Talk;
import OnlineShoppingSystem.cleanCode.items.Item;
import OnlineShoppingSystem.cleanCode.user.User;

import java.util.List;

public class ItemView extends View {

    public ItemView(Menu menu, Talk communicationChannel) {
        super(menu, communicationChannel);
    }

    public void viewItemMenu(List<Item> items, User user) {
        String itemMenu = menu.constructItemMenu(items);

        int maximumOptionValue = items.size() - 1;
        int optionValue = getEnteredOption(itemMenu, maximumOptionValue);

        viewItemInfo(optionValue, items, user);
    }

    private void viewItemInfo(int optionValue, List<Item> items, User user) {
        String itemInformation = items.get(optionValue).getInformation();
        communicationChannel.print(itemInformation);

        String acceptanceMenu = menu.constructAcceptanceShoppingCartMenu();

        int maximumOptionValue = 1;
        int option = getEnteredOption(acceptanceMenu, maximumOptionValue);

        if (option == 1) {
            user.addItemToShoppingCart(items.get(optionValue));
        }
    }
}
