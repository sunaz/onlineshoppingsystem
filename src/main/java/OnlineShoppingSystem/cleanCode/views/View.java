package OnlineShoppingSystem.cleanCode.views;

import OnlineShoppingSystem.cleanCode.Menu;
import OnlineShoppingSystem.cleanCode.Talk;

public class View {
    
    protected Menu menu;
    protected Talk communicationChannel;
    
    public View(Menu menu, Talk communicationChannel) {
        this.communicationChannel = communicationChannel;
        this.menu = menu; 
    }
    
    protected int getEnteredOption(String menu, int maximumOptionValue) {
        int minimalOptionValue = 0;

        int option = communicationChannel.enterMenuOption(menu,
                minimalOptionValue, maximumOptionValue);
        return option;
    }
}
