package OnlineShoppingSystem.cleanCode.views;

import java.util.List;

import OnlineShoppingSystem.cleanCode.Menu;
import OnlineShoppingSystem.cleanCode.Talk;
import OnlineShoppingSystem.cleanCode.items.Item;
import OnlineShoppingSystem.cleanCode.user.User;

public class ShoppingCartView extends View {

    public ShoppingCartView(Menu menu, Talk communicationChannel) {
        super(menu, communicationChannel);
    }

    public void viewShoppingCart(List<Item> items, User user) {
        String shoppingCartMenu = menu.constructInitialShoppingCartMenu();

        int maximumOptionValue = 2;
        int optionValue = getEnteredOption(shoppingCartMenu,
                maximumOptionValue);

        if (optionValue == 1) {
            viewPaymentWays(user);
        } else if (optionValue == 2) {
            viewRemoveItemMenu(items, user);
        }
    }

    private void viewPaymentWays(User user) {
        String paymentMenu = menu.constructPaymentMenu();

        user.buyItems(communicationChannel, paymentMenu);
    }

    private void viewRemoveItemMenu(List<Item> items, User user) {
        String removeItemMenu = menu.constructRemoveItemMenu(items);
        communicationChannel.print(removeItemMenu);

        String optionID = communicationChannel.getOptionID();
        user.removeItemFromShoppingCart(optionID);
    }
}
