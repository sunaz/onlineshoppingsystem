package OnlineShoppingSystem.cleanCode;

import OnlineShoppingSystem.cleanCode.items.Item;
import java.util.List;

public class Menu {

    public String constructInitialMenu() {
        String initialMenu = "";

        initialMenu = "0: Exit\n" + "1: View Items\n" + "2: View ShoppingCart\n"
                + "Enter number from 0 to 2: ";
        return initialMenu;
    }

    public String constructItemMenu(List<Item> items) {
        String itemMenu = "";
        itemMenu = addItemsToMenu(items).toString();

        return itemMenu;
    }

    public String constructAcceptanceShoppingCartMenu() {
        String acceptanceMenu = "";

        acceptanceMenu = "Add item to Shopping Cart:\n" + "0: Cancel\n"
                + "1: Accept\n" + "Enter 0 or 1: ";
        return acceptanceMenu;
    }

    public String constructInitialShoppingCartMenu() {
        String initialShoppingCartMenu = "";

        initialShoppingCartMenu = "0: Go to Home Menu\n" + "1: Buy Items\n"
                + "2: Delete item\n" + "Enter a number from 0 to 2: ";
        return initialShoppingCartMenu;
    }

    public String constructPaymentMenu() {
        String paymentMenu = "";

        paymentMenu = "Choose payment method:\n" + "0: Credit card\n"
                + "1: Debit card\n" + "Enter 0 or 1: ";
        return paymentMenu;
    }

    public String constructRemoveItemMenu(List<Item> items) {
        String removeItemMenu = "";

        removeItemMenu = addItemInformation(items).toString();

        return removeItemMenu;
    }

    private StringBuilder addItemsToMenu(List<Item> items) {
        StringBuilder allItems = new StringBuilder();

        if (items.size() > 0) {
            allItems.append("Items:\n");
            for (int i = 0; i < items.size(); i++) {
                allItems.append(i + ": " + items.get(i).getName() + "\n");
            }

            int firstProductIndex = 0;
            int lastProductIndex = items.size() - 1;
            allItems.append("Eneter a number from " + firstProductIndex + " to "
                    + lastProductIndex + ": ");
        }

        return allItems;
    }

    private StringBuilder addItemInformation(List<Item> items) {
        StringBuilder itemInformation = new StringBuilder();

        if (items.size() > 0) {
            itemInformation.append("Items in Shopping Cart:\n");
            for (int i = 0; i < items.size(); i++) {
                itemInformation.append(items.get(i).getInformation());
            }
            itemInformation
                    .append("Enter ID to remove product from ShoppingCart: ");
        }
        return itemInformation;
    }
}