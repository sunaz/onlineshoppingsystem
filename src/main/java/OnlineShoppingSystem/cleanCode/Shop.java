package OnlineShoppingSystem.cleanCode;

import OnlineShoppingSystem.cleanCode.items.Bag;
import OnlineShoppingSystem.cleanCode.items.Colors;
import OnlineShoppingSystem.cleanCode.items.Item;
import OnlineShoppingSystem.cleanCode.items.ShoeSize;
import OnlineShoppingSystem.cleanCode.items.Shoes;
import OnlineShoppingSystem.cleanCode.items.Types;
import OnlineShoppingSystem.cleanCode.user.User;
import OnlineShoppingSystem.cleanCode.views.ItemView;
import OnlineShoppingSystem.cleanCode.views.ShoppingCartView;

import java.util.ArrayList;
import java.util.List;

public class Shop {

    private Talk communicationChannel;
    private User user;
    private List<Item> items;
    private Menu menu;

    public Shop(Talk communicationChannel) {
        items = new ArrayList<Item>();
        createItems();
        this.communicationChannel = communicationChannel;
        createUser();

        this.menu = new Menu();
    }

    public void startShopping() {
        String welcomeMessage = "WELCOME!\n";
        communicationChannel.print(welcomeMessage);

        String initialMenu = menu.constructInitialMenu();
        int optionValue = getEnteredOption(initialMenu);

        while (optionValue != 0) {
            if (optionValue == 1) {
                ItemView itemView = new ItemView(menu, communicationChannel);
                itemView.viewItemMenu(items, user);

            } else {
                ShoppingCartView cartView = new ShoppingCartView(menu,
                        communicationChannel);
                cartView.viewShoppingCart(items, user);
            }
            optionValue = getEnteredOption(initialMenu);
        }
        stopShopping();
    }

    private void createUser() {
        String username = "user";
        String email = "user@gmail.com";
        user = new User(username, email);
    }

    private void stopShopping() {
        String exitMessage = "GOODBYE!";
        communicationChannel.print(exitMessage);
    }

    private int getEnteredOption(String menu) {
        int minimalOptionValue = 0;
        int maximumOptionValue = 2;

        int option = communicationChannel.enterMenuOption(menu,
                minimalOptionValue, maximumOptionValue);
        return option;
    }

    private void createItems() {
        createDress();
        createBags();
        createShoes();
    }

    private void createBags() {
        int numberOfBags = 5;
        Colors[] colors = Colors.values();
        Types[] types = Types.values();
        Bag bag;

        for (int i = 1; i <= numberOfBags; i++) {
            String name = "Bag " + i;
            float price = 20.99f * i;
            Colors color = colors[i % colors.length];
            String description = "Beatiful bag";
            Types type = types[i % types.length];

            bag = new Bag(name, price, color, description, type);
            items.add(bag);
        }
    }

    private void createShoes() {
        int numberOfShoes = 5;
        Colors[] colors = Colors.values();
        ShoeSize[] sizes = ShoeSize.values();
        Shoes shoes;

        for (int i = 1; i <= numberOfShoes; i++) {
            String name = "Shoes " + i;
            float price = 10.99f * i;
            Colors color = colors[i % colors.length];
            String description = "Modern shoes";
            ShoeSize size = sizes[i % sizes.length];

            shoes = new Shoes(name, price, color, description, size);
            items.add(shoes);
        }
    }

    private void createDress() {
        int numberOfDress = 10;
        Colors[] colors = Colors.values();
        Types[] sizes = Types.values();
        Bag dress;

        for (int i = 1; i <= numberOfDress; i++) {
            String name = "Dress " + i;
            float price = 9.99f * i;
            Colors color = colors[i % colors.length];
            String description = "Everyday dress";
            Types size = sizes[i % sizes.length];

            dress = new Bag(name, price, color, description, size);
            items.add(dress);
        }
    }
}
