package OnlineShoppingSystem.cleanCode.cart;

import OnlineShoppingSystem.cleanCode.items.Item;
import java.util.ArrayList;
import java.util.List;


public class ShoppingCart {

    private List<Item> items;

    public ShoppingCart() {
        items = new ArrayList<Item>();
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public boolean removeItem(Item item) {
        boolean isRemoved = items.remove(item);
        return isRemoved;
    }

    public void clear() {
        items.clear();
    }

    public void removeItemByID(String optionID) {
        for (int i = items.size() - 1; i >= 0; i--) {
            if (items.get(i).getID().equals(optionID)) {
                items.remove(i);
            }
        }
    }
}
