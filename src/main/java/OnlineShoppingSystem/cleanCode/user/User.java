package OnlineShoppingSystem.cleanCode.user;

import OnlineShoppingSystem.cleanCode.Payment;
import OnlineShoppingSystem.cleanCode.Talk;
import OnlineShoppingSystem.cleanCode.cart.ShoppingCart;
import OnlineShoppingSystem.cleanCode.items.Item;

public class User {
    private String username;
    private String email;
    private ShoppingCart shoppingCart;

    public User(String username, String email) {
        this.username = username;
        this.email = email;
        this.shoppingCart = new ShoppingCart();
    }

    public void addItemToShoppingCart(Item item) {
        shoppingCart.addItem(item);
    }

    public void buyItems(Talk communicationChannel, String paymentMenu) {
        buy(communicationChannel, paymentMenu);
        shoppingCart.clear();
    }

    private void buy(Talk communicationChannel, String paymentMenu) {
        int minimalOptionValue = 0;
        int maximumOptionValue = 1;
        int optionValue = communicationChannel.enterMenuOption(paymentMenu,
                minimalOptionValue, maximumOptionValue);

        Payment paymentMethod = new Payment(communicationChannel);
        if (optionValue == minimalOptionValue) {
            paymentMethod.payWithCreditCard();
        } else {
            paymentMethod.payWithDebitCard();
        }
    }

    public void removeItemFromShoppingCart(String optionID) {
        shoppingCart.removeItemByID(optionID);
    }
}
