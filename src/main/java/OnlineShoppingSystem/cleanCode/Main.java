package OnlineShoppingSystem.cleanCode;

public class Main {

    public static void main(String[] args) {
        Talk communicationChannel = new Talk();

        Shop shop = new Shop(communicationChannel);
        shop.startShopping();
    }
}
